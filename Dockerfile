# syntax=docker/dockerfile:1

FROM alpine

RUN apk add --no-cache curl git skopeo jq yq coreutils

COPY scripts/ /uc/v1/
