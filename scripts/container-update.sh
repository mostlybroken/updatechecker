#!/bin/sh

# set -euo pipefail

# tag::doc[]
#   Usage: . /uc/v1/container-update.sh sourcecontainer targetcontainer [var_name]
#   Checks the time stamps source and target.
#   Sets 'var_name' to 'yes' if source is newer then target
#   Default for 'var_name' is 'updates_found'.
# end::doc[]

# skopeo inspect --format '{{ .Created.UTC.Unix }}' docker://docker.io/library/docker:24
upstr_date="$(skopeo inspect --format '{{ .Created.UTC.Unix }}' docker://${1} 2>&1)" || exit_code=$?
if [ -n "${exit_code:-}" ]; then
  echo "Something went wrong while checking container age ($exit_code):"
  echo $upstr_date
  exit $exit_code
fi;

target_date="$(skopeo inspect --format '{{ .Created.UTC.Unix }}' docker://${2} 2>&1)" || exit_code=$?
if [ -n "${exit_code:-}" ]; then
  if ! echo "$target_date" | grep "manifest unknown"; then
    echo "Something went wrong while checking container age ($exit_code):"
    echo "$target_date"
    exit $exit_code
  else
    echo "Manifest unknown: 404. Container may not exist yet."
    unset target_date
    target_date=0
  fi
fi

if [ "$upstr_date" -gt "$target_date" ]; then
  echo "Upstream is newer. Rebuild. (${upstr_date} > ${target_date})."
  eval ${3:-"updates_found"}="yes"
else
  echo "Upstream is older. No need to rebuild. (${upstr_date} < ${target_date})."
fi
