#!/bin/sh

# set -euo pipefail

# tag::doc[]
#   Usage . ./uc/v1/github-latest.sh user repos version [var_name]
#   Checks the latest stable release from github to be 'version',
#   otherwise 'var_name' is set to 'yes'.
#   Default for 'var_name' is 'updates_found'.
# end::doc[]


gh_latest=$(curl -sSL https://api.github.com/repos/$1/$2/releases | jq -r '.[] | select(.prerelease == false and .draft == false) | .name' | sort -V -r | head -1)
if [ "$gh_latest" != "$3" ]; then
  echo "Unexpected version for $1 $2: $gh_latest (expected $3)"
  eval ${4:-"updates_found"}="yes"
else
  echo "Found expected version for $1 $2: $gh_latest (expected $3)"
fi
