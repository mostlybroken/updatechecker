#!/bin/sh

# set -euo pipefail

# tag::doc[]
#   Usage: . /uc/v1/alpine-update.sh [var_name]
#   Checks for alpine updates, if updates are aviable they
#   are printed to std out and 'var_name' is set to 'yes'.
#   Default for 'var_name' is 'updates_found'.
# end::doc[]


echo "Checking Alpine update…"
apk update
alp_upd=$(apk -u list)
if [ -n "${alp_upd:-}" ]; then
  echo -e "Updates for packages:\n${alp_upd})."
  eval ${1:-"updates_found"}="yes"
fi
